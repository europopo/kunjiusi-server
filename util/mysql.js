const mysql = require('mysql2');

const connection = mysql.createConnection({
    host: '112.124.19.57',
    user: 'europe',
    password: 'europe',
    database: 'kunjiusi'
});

const conn = connection.promise();

exports.query = async (sql) =>{
    const [rows, fields] = await conn.query(sql);
    return rows;
}
