const Minio = require('minio');

exports.minioClient = new Minio.Client({
    endPoint: '112.124.19.57',
    port: 9000,
    useSSL: false,
    accessKey: 'miniouser',
    secretKey: 'miniouser',
});