const {sequelize} = require('../db');
const { DataTypes } = require("sequelize");

// 定义数据模型
const OrderList = sequelize.define("order_list", {
    orderId: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    totalPrice: {
        type: DataTypes.FLOAT
    },
    orderTime: {
        type: DataTypes.DATE
    },
    openid: {
        type: DataTypes.STRING(100)
    },
    addressId: {
        type: DataTypes.INTEGER
    }
  });

  module.exports = {
    OrderList
}