const {sequelize} = require('../db');
const { DataTypes } = require("sequelize");

// 定义数据模型
const Address = sequelize.define("address", {
    // id: {
    //   type: DataTypes.INTEGER,
    //   allowNull: false,
    //   autoIncrement: true,
    //   primaryKey: true
    // },
    openid: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    selected: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING(15)
    },
    sex: {
        type: DataTypes.STRING(2),
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING(11)
    },
    detail: {
        type: DataTypes.STRING(20)
    },
    tag: {
        type: DataTypes.STRING(2)
    },
    addressId: {
        type: DataTypes.STRING(50)
    },
    title: {
        type: DataTypes.STRING(20)
    },
    address: {
        type: DataTypes.STRING(100)
    },
    latitude: {
        type: DataTypes.FLOAT
    },
    longitude: {
        type: DataTypes.FLOAT
    },
    createTime: {
        type: DataTypes.DATE
    },
    updateTime: {
        type: DataTypes.DATE
    }
});

module.exports = {
    Address
}

