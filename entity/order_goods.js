const {sequelize} = require('../db');
const { DataTypes } = require("sequelize");

// 定义数据模型
const OrderGoods = sequelize.define("order_goods", {
    // id: {
    //   type: DataTypes.INTEGER,
    //   allowNull: false,
    //   autoIncrement: true,
    //   primaryKey: true
    // },
    orderId: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    goodsId: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    title: {
        type: DataTypes.STRING(50)
    },
    amount: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    discount: {
        type: DataTypes.FLOAT
    },
    isDiscount: {
        type: DataTypes.TINYINT
    },
    finalPrice: {
        type: DataTypes.FLOAT
    }
});

module.exports = {
    OrderGoods
}