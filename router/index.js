const router = require('express').Router();
// const {indexData} = require('../mock/index');
const {getIndexData} = require('../main/index');
const {getMenuData} = require('../main/menu');
const menuData = require('../mock/menu');
const orderData = require('../mock/order');
const addressData = require('../mock/address');
const {getOpneid} = require('../main/confirmOrder');
const {insertOrder, selectOrderAll} = require('../main/order');
const {insertAddress, getAddress, updateSelected, deleteAddress} = require('../main/address');

router.get('/', (req, res)=>{
    res.send('hello world')
})

router.get('/index', async (req, res)=>{
    res.send(await getIndexData());
});

router.get('/menu', async (req, res)=>{
    res.send(await getMenuData());
})

router.get('/order', async (req, res)=>{
    res.send(await selectOrderAll(req.query.openid))
})

router.post('/order', async (req, res)=>{
    res.send(await insertOrder(req.body));
})

router.get('/address', async (req, res)=>{
    res.send(await getAddress(req.query.openid))
})

router.post('/address', async (req, res)=>{
    res.send(await insertAddress(req.body));
});

router.put('/address', async (req, res)=>{
    res.send(await updateSelected(req.body.openid, req.body.id));
});

router.delete('/address', async (req, res)=>{
    res.send(await deleteAddress(req.body.id));
})

router.post('/getOpneid', async (req, res)=>{
    res.send(await getOpneid(req.body.code))
})


module.exports = router;