const Mock = require('mockjs');

const data = Mock.mock({
    'menuData|8': [{
        category: Mock.Random.ctitle(2,5),
        'categoryID|+1': 1,
        'goodsList|7-14': [{
            'goodsID|+1': 1,
            title: Mock.Random.ctitle(4,10),
            desc: Mock.Random.csentence(10,30),
            'finalPrice|1-100.2': 1,
            'discount|1-100.2': 1,
            amount: 0,
        }]
    }] 
})


module.exports = data;