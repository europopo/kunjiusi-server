const Mock = require('mockjs');

module.exports = Mock.mock({
    'addressData|3-15': [{
        'id|+1': 1,
        name: Mock.Random.cname(),
        'sex|1': ['先生', '女士'],
        phone: 17802585363,
        detail: '4栋404',
        'tag|1': ['家', '公司', '学校', ''],
        addressInfo: {
            title: "宜家花园4栋",
            address: "江苏省苏州市昆山市金阳西路宜家花园",
            location: {
                'latitude|0-99.3': 1,
                'longitude|0-99.3': 1,
            }
        }
    }]
})