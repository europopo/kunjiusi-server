const Mock = require('mockjs');

module.exports = Mock.mock({
    'orderData|1-8': [{
        orderTime: Mock.Random.datetime("yyyy-MM-dd hh:mm:ss"),
        'orderID|+1': 1,
        'goodsList|1-3': [{
            'goodsID|+1': 1,
            title: Mock.Random.ctitle(4, 10),
            'amount|1-10': 1,
            'finalPrice|10-100.2': 1,
            'discount|50-100.2': 1,
        }],
        'totalPrice|10-100': 1
    }]
});