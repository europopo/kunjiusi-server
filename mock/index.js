const Mock = require('mockjs');

module.exports = {
    indexData: Mock.mock({
        'topImags|5': [{
            'id|+1': 1,
            imgUrl: Mock.Random.image('320x200', '#FF6600')
        }],
        'actImages|5': [{
            'id|+1': 1,
            imgUrl: Mock.Random.image('320x150', '#FF6600')
        }]
    }),
    
}