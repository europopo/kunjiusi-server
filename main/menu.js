const {query} = require('../util/mysql');
const {minioClient} = require('../util/minio');

module.exports = {
    async getMenuData() {
        const data = await query('SELECT a.*, b.title as category FROM goods a JOIN conf_cate_main b ON a.cid=b.uid  ORDER BY b.sortid asc');
        const res = [];
        for (const item of data) {
            const picUrl = await minioClient.presignedGetObject(
                'images',
                item.picUrl,
            );
            const descPicUrl = await minioClient.presignedGetObject(
                'images',
                item.picUrl,
            );
            const group = res.find(f=>f.categoryID==item.cid);
            const goodsInfo = {
                goodsId: item.id,
                picUrl,
                descPicUrl,
                title: item.title,
                desc: item.desc,
                finalPrice: item.finalPrice,
                discount: item.discount,
                amount: 0,
            };
            if (group) {
                group.goodsList.push(goodsInfo)
            } else {
                res.push({
                    category: item.category,
                    categoryID: item.cid,
                    goodsList: [goodsInfo]
                })
            }
        }
        return res;
    }
}