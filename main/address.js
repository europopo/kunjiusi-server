const {Address} = require('../entity/address')
const {sequelize, Op} = require("../db");
const { v4: uuidv4 } = require('uuid');

module.exports = {
    /**
     * 写入地址信息
     * @param {*} addressInfo 
     */
    async insertAddress(addressInfo) {
        try {
            
            await Address.upsert({
                id: addressInfo.id,
                openid: addressInfo.openid,
                selected: addressInfo.selected,
                detail: addressInfo.detail,
                tag: addressInfo.tag,
                name: addressInfo.name,
                sex: addressInfo.sex,
                phone: addressInfo.phone,
                addressId: addressInfo.addressId,
                title: addressInfo.title,
                address: addressInfo.address,
                latitude: addressInfo.location.latitude,
                longitude: addressInfo.location.longitude
            });

            // const data = await this.getAddress(addressInfo.openid);
            
            return {code: 1, msg: 'ok'};
        } catch (error) {
            console.log(error);
        }
    },

    /**
     * 獲取用戶地址信息
     * @param {*} openid 
     * @returns 
     */
    async getAddress(openid) {
        const addressList = await Address.findAll({where: {openid}, order: [['updateTime', 'DESC'], ['createTime', 'DESC']]});
        return addressList.reduce((a, c)=>{
            a.push({
                id: c.id,
                openid: c.openid,
                selected: c.selected,
                name: c.name,
                sex: c.sex,
                phone: c.phone,
                detail: c.detail,
                tag: c.tag,
                addressId: c.addressId,
                title: c.title,
                address: c.address,
                location: {
                    latitude: c.latitude,
                    longitude: c.longitude
                }
                
            })
            return a;
        }, []);

    },

    /**
     * 更新已选择地址
     * @param {*} openid 
     * @param {*} id 
     */
    async updateSelected(openid, id) {
        const t = await sequelize.transaction();
        try {
            await Address.update({selected: 1}, {
                where: {
                    openid,
                    id
                }
            });
            await Address.update({selected: 0}, {
                where: {
                    openid,
                    id: {
                        [Op.ne]: id
                    }
                }
            });
            
            await t.commit();
            return {code: 1, msg: 'ok'}

        } catch (error) {
            console.log(error);
            await t.rollback();
        }
    },

    async deleteAddress(id) {
        if (id) {
            await Address.destroy({
                where: {id}
            });
            return {code: 1, msg: 'ok'}
        } else {
            return {code: 0, msg: 'id is null'}
        }
        
    } 

}