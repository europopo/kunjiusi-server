const {OrderList} = require("../entity/order_list");
const {OrderGoods} = require("../entity/order_goods");
const {sequelize} = require("../db");
const { v4: uuidv4 } = require('uuid');

module.exports = {

    async selectOrderAll(openid) {
        const res = [];
        if(openid) {

            const orderList = await OrderList.findAll({
                where: { openid }
            });
            const orderIdList = orderList.map(m=>m.orderId);
            const orderGoods = await OrderGoods.findAll({where: {orderId: orderIdList}});
            orderList.forEach(ol=>{
                const goods = orderGoods.filter(f=>f.orderId==ol.orderId);
                res.push({
                    ...ol.dataValues,
                    goodsList: goods
                });
            })

        }
        
        return res;

    },

    async insertOrder(data) {
        const orderId = uuidv4();

        const orderlist = {
            orderId,
            openid: data.openid,
            totalPrice: data.totalPrice,
            orderTime: data.orderTime,
            addressId: data.addressId,
        }

        const ordergoodsList = [];
        for (const goods of data.goodsList) {
            ordergoodsList.push({
                orderId,
                goodsId: goods.goodsId,
                title: goods.title,
                amount: goods.amount,
                discount: goods.discount,
                isDiscount: goods.isDiscount,
                finalPrice: goods.finalPrice,
            });
        } 
        const t = await sequelize.transaction();
        try {
            await OrderList.create(orderlist);
            await OrderGoods.bulkCreate(ordergoodsList);
            await t.commit();
            return {code: 1, msg: "ok"};
        } catch (error) {
            console.log(error);
            await t.rollback();
        }
        

    }
}