const {query} = require('../util/mysql');
const {minioClient} = require('../util/minio');

module.exports = {
    async getIndexData() {
        const advertImg = await query('select * from advert_img');
        const res = {};
        for (const item of advertImg) {
            const url = await minioClient.presignedGetObject(
                'images',
                item.picUrl,
            );

            if (!res[item.folder]) {
                res[item.folder] = [];
            }

            res[item.folder].push({
                name: item.name,
                url,
            });
        }
        return res;
    }
}

